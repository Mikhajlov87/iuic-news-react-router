import React from 'react';
import ButtonWithSearchField from './assets/components/buttons/buttonWithSearchField/ButtonWithSearchField';

const styles = {
  buttonContainer: {
    display: "flex",
    justifyContent: "center"
  },
  h2: {
    textAlign: "center"
  }
}

const CountryPage = () => (
  <div>
    <div style={styles.buttonContainer}>
      <ButtonWithSearchField>моя страна</ButtonWithSearchField>
      <ButtonWithSearchField>города страны</ButtonWithSearchField>
    </div>
    <h2 style={styles.h2}>Country Page</h2>
  </div>
)

export default CountryPage;