import React from 'react';
import { Link } from 'react-router-dom';
import logo from './assets/images/logo-news.png';
import { headerBackground, headerMenuButton } from './assets/colors';
import IconButton from 'material-ui/IconButton';
import NavigationMenu from 'material-ui/svg-icons/navigation/menu';

const styles = {
  header: {
    alignItems: "center",
    backgroundColor: headerBackground,
    boxSizing: "border-box",
    display: "flex",
    height: 90,
    justifyContent: "center",
    padding: "10px 20px"
  },
  link: {
    marginRight: "auto",
    width: 210
  },
  logo: {
    height: "auto",
    maxWidth: "100%"
  },
  icon: {
    color: headerMenuButton
  }
}

const Header = () => (
  <div style={styles.header}>
    <IconButton
      iconStyle={styles.icon}>
      <NavigationMenu />
    </IconButton>
    <Link style={styles.link} to="/">
      <img style={styles.logo} src={logo} alt="IUIC" />
    </Link>
  </div>
)

export default Header;