import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Header from './Header';
import Main from './Main';
import ToolBar from './ToolBar';
import News from './News';
import Footer from './Footer';
import WorldPage from './WorldPage';
import CountryPage from './CountryPage';
import CityPage from './CityPage';
import './styles/main.css';

const App = () => (
  <Router>
    <div>
      <Header />
      <Main>
        <Route exact path="/" component={WorldPage}/>
        <Route path="/country" component={CountryPage}/>
        <Route path="/city" component={CityPage}/>
      </Main>
      <ToolBar />
      <News>
        <Route exact path="/" component={WorldNews}/>
        <Route path="/country" component={CountryNews}/>
        <Route path="/city" component={CityNews}/>
      </News>
      <Footer />
    </div>
  </Router>
)

const WorldNews = () => (
  <div>
    <h2>World News</h2>
  </div>
)

const CountryNews = () => (
  <div>
    <h2>Country News</h2>
  </div>
)

const CityNews = () => (
  <div>
    <h2>City News</h2>
  </div>
)

export default App;