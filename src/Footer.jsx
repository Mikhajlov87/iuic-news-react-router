import React from 'react';

const styles = {
  footer: {
    alignItems: "center",
    backgroundColor: "#cdcdcd",
    border: "1px solid #000",
    display: "flex",
    height: 90,
    justifyContent: "center",
    marginTop: 5
  }
}

const Footer = () => (
  <div style={styles.footer}>footer</div>
)

export default Footer;