import React from 'react';

const styles = {
  h2: {
    margin: 0,
    textAlign: "center"
  }
}

const CityPage = () => (
  <div>
    <h2 style={styles.h2}>City Page</h2>
  </div>
)

export default CityPage;