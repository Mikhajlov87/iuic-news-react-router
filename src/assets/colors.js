//HEADER
export const headerBackground = "#d9edf7";
export const headerMenuButton = "#808080";

//NAVBAR
export const navbarBackground = "#bccecf";
export const navbarLinkColor = "#054d50";

//MAIN
export const mainColor = "#00525e";
export const white = "#ffffff";

//BUTTON_WITH_SEARCH_FIELD
export const buttonBgColor = "rgba(0, 0, 0, 0.2)";