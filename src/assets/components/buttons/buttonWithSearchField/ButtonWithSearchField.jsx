import React from 'react';
import DropDownMenuButton from '../dropDownMenu/DropDownMenu';
import './search-field-button.css';

const ButtonWithSearchField = (props) => (
  <div className="search-field-button">{props.children}</div>
)

export default ButtonWithSearchField;