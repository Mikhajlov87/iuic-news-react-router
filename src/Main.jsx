import React from 'react';

const styles = {
  main: {
    backgroundColor: "#cdcdcd",
    height: 300
  }
}

const Main = (props) => (
  <div style={styles.main}>
    {props.children}
  </div>
)

export default Main;