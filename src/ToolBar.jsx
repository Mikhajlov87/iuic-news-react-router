import React from 'react';
import { NavLink } from 'react-router-dom';
import { navbarBackground, navbarLinkColor } from './assets/colors';

const styles = {
  toolbar: {
    background: navbarBackground,
    height: 50,
    padding: "0 20px"
  },
  ul: {
    alignItems: "center",
    display: "flex",
    height: "inherit",
    padding: 0,
    margin: 0,
    listStyleType: "none"
  },
  li: {
    height: "inherit"
  },
  a: {
    alignItems: "center",
    color:navbarLinkColor,
    font: '500 14px "Roboto", sans-serif',
    display: "flex",
    height: "inherit",
    justifyContent: "center",
    textDecoration: "none",
    padding: "0 15px"
  }
}

const ToolBar = () => (
  <div style={styles.toolbar}>
    <ul style={styles.ul}>
      <li style={styles.li}>
        <NavLink style={styles.a} exact to="/">В мире</NavLink>
      </li>
      <li style={styles.li}>
        <NavLink style={styles.a} to="/country">В стране</NavLink>
      </li>
      <li style={styles.li}>
        <NavLink style={styles.a} to="/city">Мой город</NavLink>
      </li>
    </ul>
  </div>
)

export default ToolBar;